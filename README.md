# simonbreiter

[![pipeline status](https://gitlab.com/simonbreiter/simonbreiter/badges/master/pipeline.svg)](https://gitlab.com/simonbreiter/simonbreiter/-/commits/master)

Code of [simonbreiter.com](https://simonbreiter.com).

## Development

Start development with:

```bash
npm run dev
```

This will start a local dev environment running on http://localhost:3000.

## Troubleshooting

Print out helm chart:

```bash
npm run helm:debug
```
