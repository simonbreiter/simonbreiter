import fs from "fs"

type Slug = {
  params: {
    slug: string
  }
}

type Slugs = Slug[]

export function getSlugsFromDir(path: string): Slugs {
  const fileNames = fs.readdirSync(path)
  return fileNames.map((fileName) => {
    return {
      params: {
        slug: fileName.replace(/\.mdx$/, ""),
      },
    }
  })
}
