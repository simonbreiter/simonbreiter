import type { NextPage } from "next"
import Link from "next/link"
// import styles from "@styles/index.module.css"
import fs from "fs"
import listStyles from "@styles/list.module.css"
import matter from "gray-matter"
import { motion } from "framer-motion"
import orderBy from "lodash/orderBy"
import Head from "next/head"

type FrontMatter = {
  title: string
  date: string
  description: string
  thumbnailUrl: string
  skills: string[]
}

type Project = {
  slug: string
  frontMatter: FrontMatter
}

type Props = {
  projects: Project[]
}

const liVariants = {
  hidden: { opacity: 0, x: 0, y: 100 },
  enter: (i: number) => ({
    opacity: 1,
    x: 0,
    y: 0,
    transition: {
      type: "spring",
      damping: 15,
      mass: 0.1,
      stiffness: 250,
      delay: i * 0.05,
    },
  }),
  // hover: {
  //   backgroundColor: "#fff",
  //   transition: { duration: 0.1, type: "tween" },
  // },
}

const ListItem = ({ project, i }: { project: Project; i: number }) => (
  <li>
    <Link href={`/posts/${project.slug}`}>
      <motion.a
        custom={i}
        variants={liVariants}
        whileHover="hover"
        initial="hidden"
        animate="enter"
        key={i}
        className={listStyles.listItem}
      >
        <article>
          <header className={listStyles.listItemHeader}>
            <h1 className={listStyles.listItemTitle}>
              {project.frontMatter.title}
            </h1>
            <time
              className={listStyles.listItemDate}
              dateTime={project.frontMatter.date}
            >
              {project.frontMatter.date}
            </time>
          </header>
          <div>{project.frontMatter.description}</div>
        </article>
      </motion.a>
    </Link>
  </li>
)

const Index: NextPage<Props> = ({ projects }) => {
  return (
    <div>
      <Head>
        <title>Simon Breiter | Posts</title>
        <meta name="description" content="Things I've learned on my way." />
      </Head>
      <ul className={listStyles.list}>
        {orderBy(projects, "frontMatter.date", "desc").map(
          (project: Project, i: number) => (
            <ListItem key={i} project={project} i={i} />
          )
        )}
      </ul>
    </div>
  )
}

export async function getStaticProps() {
  const fileNames = fs.readdirSync("./docs/posts")
  const projects = fileNames.map((fileName) => {
    const markdownWithMeta = fs.readFileSync(`./docs/posts/${fileName}`, "utf8")
    const { data } = matter(markdownWithMeta)

    return {
      slug: fileName.replace(/\.mdx$/, ""),
      frontMatter: data,
    }
  })

  return {
    props: {
      projects: projects,
    },
  }
}

export default Index
