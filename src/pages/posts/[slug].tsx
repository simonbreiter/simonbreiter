import { serialize } from "next-mdx-remote/serialize"
import { MDXRemote, MDXRemoteSerializeResult } from "next-mdx-remote"
import matter from "gray-matter"
import remarkPrism from "remark-prism"
import remarkGfm from "remark-gfm"
import fs from "fs"
import { MDXComponents } from "mdx/types"
import detailPageStyles from "@styles/detailPage.module.css"
import Head from "next/head"

type FrontMatter = {
  title: string
  date: string
  description: string
  thumbnailUrl: string
  skills: string[]
}

type PostProps = {
  frontMatter: FrontMatter
  source: JSX.IntrinsicAttributes &
    MDXRemoteSerializeResult<Record<string, unknown>> & {
      components?: MDXComponents | undefined
      lazy?: boolean | undefined
    }
}

export default function Post(props: PostProps) {
  return (
    <div>
      <Head>
        <title>Simon Breiter | Posts | {props.frontMatter.title}</title>
        <meta name="description" content={props.frontMatter.description} />
      </Head>
      <div className={detailPageStyles.detailPage}>
        <div className={detailPageStyles.header}>
          <h1 className={detailPageStyles.title}>{props.frontMatter.title}</h1>
          <time
            className={detailPageStyles.date}
            dateTime={props.frontMatter.date}
          >
            {new Date(Date.parse(props.frontMatter.date)).toLocaleDateString(
              "de-DE"
            )}
          </time>
        </div>
        <MDXRemote {...props.source} />
      </div>
    </div>
  )
}

type Params = {
  params: {
    slug: string
  }
}

type Slugs = Array<Params>

function getAllPostSlugs(): Slugs {
  const fileNames = fs.readdirSync("./docs/posts")
  return fileNames.map((fileName) => {
    return {
      params: {
        slug: fileName.replace(/\.mdx$/, ""),
      },
    }
  })
}

// https://github.com/hashicorp/next-mdx-remote
// https://github.com/ebenezerdon/nextjs-mdx-blog/blob/main/pages/blog/%5Bslug%5D.js

// Possible paths
export async function getStaticPaths() {
  const slugs = getAllPostSlugs()
  return {
    paths: slugs,
    fallback: false,
  }
}

// Get data for path
export async function getStaticProps({ params }: { params: { slug: string } }) {
  const markdownWithMeta = fs.readFileSync(
    `./docs/posts/${params.slug}.mdx`,
    "utf8"
  )
  const { data, content } = matter(markdownWithMeta)
  const mdxSource = await serialize(content, {
    mdxOptions: {
      remarkPlugins: [remarkPrism, remarkGfm],
    },
  })

  return {
    props: {
      frontMatter: data,
      source: mdxSource,
    },
  }
}
