import "../styles/globals.css"
import type { AppProps } from "next/app"
// import Layout from "@components/Layout"
// import "@styles/prism-material-dark.css"
import "@styles/prism-nord.css"
// import "@styles/prism-one-light.css"
import styles from "@styles/layout.module.css"
import { motion, AnimatePresence } from "framer-motion"
import Head from "next/head"
import Menu from "@components/Menu"
import Footer from "@components/Footer"
import Link from "next/link"
import { usePageTransitionFix } from "@hooks/usePageTransitionFix"
import Script from "next/script"

const variants = {
  hidden: { opacity: 0, x: 0, y: 100 },
  enter: {
    opacity: 1,
    x: 0,
    y: 0,
    transition: {
      type: "spring",
      damping: 20,
      mass: 1,
      stiffness: 200,
    },
  },
  exit: {
    opacity: 0,
    x: 0,
    y: 100,
    transition: { duration: 0.1, type: "tween" },
  },
}

function App({ Component, pageProps, router }: AppProps) {
  usePageTransitionFix()
  return (
    <div>
      <Script id="google-tag-manager" strategy="afterInteractive">
        {`
          (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
          new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
          j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
          'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
          })(window,document,'script','dataLayer','GTM-PR95JFW');
        `}
      </Script>
      <Head>
        <title>Simon Breiter</title>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta name="description" content="Creating software for humans." />
        {/*<link rel="icon" href="/favicon.ico" />*/}
      </Head>
      <div className={styles.appContainer}>
        <div className={styles.headerContainer}>
          <Link href={"/"}>
            <a className={styles.header}>Simon Breiter</a>
          </Link>
          <Menu />
        </div>

        <div className={styles.mainContainer}>
          <AnimatePresence exitBeforeEnter={true}>
            <motion.main
              key={router.route}
              variants={variants}
              initial="hidden"
              animate="enter"
              exit="exit"
              className={styles.main}
            >
              <Component {...pageProps} />
            </motion.main>
          </AnimatePresence>
        </div>

        <Footer />
      </div>
    </div>
  )
}

export default App
