import { serialize } from "next-mdx-remote/serialize"
import { MDXRemote, MDXRemoteSerializeResult } from "next-mdx-remote"
import matter from "gray-matter"
import remarkPrism from "remark-prism"
import fs from "fs"
import { MDXComponents } from "mdx/types"
import detailPageStyles from "@styles/detailPage.module.css"
import Head from "next/head"
import remarkGfm from "remark-gfm"

type FrontMatter = {
  title: string
  date: string
  description: string
  thumbnailUrl: string
  skills: string[]
}

type ProjectProps = {
  frontMatter: FrontMatter
  source: JSX.IntrinsicAttributes &
    MDXRemoteSerializeResult<Record<string, unknown>> & {
      components?: MDXComponents | undefined
      lazy?: boolean | undefined
    }
}

export default function ProjectPage(props: ProjectProps) {
  return (
    <div>
      <Head>
        <title>Simon Breiter | Projects | {props.frontMatter.title}</title>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta name="description" content={props.frontMatter.description} />
        {/*<link rel="icon" href="/favicon.ico" />*/}
      </Head>
      <div className={detailPageStyles.detailPage}>
        <div className={detailPageStyles.header}>
          <h1 className={detailPageStyles.title}>{props.frontMatter.title}</h1>
          <time
            className={detailPageStyles.date}
            dateTime={props.frontMatter.date}
          >
            {new Date(Date.parse(props.frontMatter.date)).toLocaleDateString(
              "de-DE"
            )}
          </time>
        </div>
        <MDXRemote {...props.source} />
      </div>
    </div>
  )
}

type Params = {
  params: {
    slug: string
  }
}

type Slugs = Array<Params>

function getAllSlugs(): Slugs {
  const fileNames = fs.readdirSync("./docs/projects")
  return fileNames.map((fileName) => {
    return {
      params: {
        slug: fileName.replace(/\.mdx$/, ""),
      },
    }
  })
}

export async function getStaticPaths() {
  const slugs = getAllSlugs()
  return {
    paths: slugs,
    fallback: false,
  }
}

export async function getStaticProps({ params }: { params: { slug: string } }) {
  const markdownWithMeta = fs.readFileSync(
    `./docs/projects/${params.slug}.mdx`,
    "utf8"
  )
  const { data, content } = matter(markdownWithMeta)
  const mdxSource = await serialize(content, {
    mdxOptions: {
      remarkPlugins: [remarkPrism, remarkGfm],
    },
  })

  return {
    props: {
      frontMatter: data,
      source: mdxSource,
    },
  }
}
