import type { NextPage } from "next"
import styles from "@styles/projectIndex.module.css"
import { motion } from "framer-motion"
// import path from "path"
import fs from "fs"
import Link from "next/link"
import matter from "gray-matter"
import orderBy from "lodash/orderBy"
import Head from "next/head"

type FrontMatter = {
  title: string
  date: string
  description: string
  thumbnailUrl: string
  skills: string[]
}

type Project = {
  slug: string
  frontMatter: FrontMatter
}

type Props = {
  projects: Project[]
}

const liVariants = {
  hidden: { opacity: 0, x: 0, y: 100 },
  enter: (i: number) => ({
    opacity: 1,
    x: 0,
    y: 0,
    transition: {
      type: "spring",
      damping: 15,
      mass: 0.1,
      stiffness: 250,
      delay: i * 0.05,
    },
  }),
  hover: {
    zIndex: 1,
    scale: [1, 1.1],
    boxShadow: "0px 10px 10px rgba(0, 0, 0, 0.2)",
    transition: {
      type: "spring",
      damping: 10,
      mass: 0.1,
      stiffness: 250,
      duration: 0.2,
    },
  },
}

const itemVariants = {
  hidden: { opacity: 0, x: 0, y: 0 },
  hover: {
    opacity: 1,
    // y: -200,
    transition: {
      type: "spring",
      damping: 10,
      mass: 0.1,
      stiffness: 250,
    },
  },
}

const backgroundVariants = {
  hover: {
    filter: "blur(15px)",
    scale: [1, 1.2],
    transition: { duration: 0.1, type: "tween" },
    // transition: {
    //   type: "spring",
    //   damping: 10,
    //   mass: 0.5,
    //   stiffness: 150,
    //   duration: 0.2,
    // },
  },
}

const ProjectListItem = ({ project, i }: { project: Project; i: number }) => (
  <motion.li
    custom={i}
    variants={liVariants}
    whileHover="hover"
    initial="hidden"
    animate="enter"
  >
    <Link href={`/projects/${project.slug}`}>
      <a>
        <motion.div className={styles.badge} variants={itemVariants}>
          <div className={styles.badgeTitle}>{project.frontMatter.title}</div>
          <div>
            {new Date(Date.parse(project.frontMatter.date)).getFullYear()}
          </div>
          <div className={styles.badgeDescription}>
            {project.frontMatter.description}
          </div>
        </motion.div>
        <motion.img
          variants={backgroundVariants}
          className={styles.badgeBackground}
          src={project.frontMatter.thumbnailUrl}
        ></motion.img>
      </a>
    </Link>
  </motion.li>
)

const Index: NextPage<Props> = ({ projects }) => {
  return (
    <div>
      <Head>
        <title>Simon Breiter | Projects</title>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta name="description" content="Creating software for humans." />
        {/*<link rel="icon" href="/favicon.ico" />*/}
      </Head>
      <div className={styles.projectIndex}>
        <ul>
          {orderBy(projects, "frontMatter.date", "desc").map(
            (project: Project, i: number) => (
              <ProjectListItem key={i} project={project} i={i} />
            )
          )}
        </ul>
      </div>
    </div>
  )
}

export async function getStaticProps() {
  const fileNames = fs.readdirSync("./docs/projects")
  const projects = fileNames.map((fileName) => {
    const markdownWithMeta = fs.readFileSync(
      `./docs/projects/${fileName}`,
      "utf8"
    )
    const { data } = matter(markdownWithMeta)

    return {
      slug: fileName.replace(/\.mdx$/, ""),
      frontMatter: data,
    }
  })

  return {
    props: {
      projects: projects,
    },
  }
}

export default Index
