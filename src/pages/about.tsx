import type { NextPage } from "next"
import detailPageStyles from "@styles/detailPage.module.css"
import emailObfuscatorStyles from "@styles/emailObfuscator.module.css"
import Head from "next/head"

const Index: NextPage = () => {
  const handleClick = () => {
    fetch("./api/email")
      .then((response) => response.json())
      .then((data) => {
        console.log(data)
        window.location.href = `mailto:${data.email}?subject=Howdy`
      })
  }
  return (
    <div>
      <Head>
        <title>Simon Breiter | About</title>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta name="description" content="Things about me." />
        {/*<link rel="icon" href="/favicon.ico" />*/}
      </Head>
      <div className={detailPageStyles.detailPage}>
        <p>
          Hi there! I&apos;m Simon, a software developer from Switzerland. I
          love learning and sharing my thoughts with others. Here is where I
          post some projects I&apos;ve made and things I&apos;ve learned along
          my way.
        </p>
        <p>
          My passions are TDD, DevOps and thinking about UX. At work, my goal is
          to help build beautiful products that scale.
        </p>
        <p>
          Feel free to add me on{" "}
          <a
            href="https://www.linkedin.com/in/simonbreiter/"
            target="_blank"
            rel="noreferrer"
          >
            LinkedIn
          </a>
          , follow me on{" "}
          <a
            href="https://gitlab.com/simonbreiter"
            target="_blank"
            rel="noreferrer"
          >
            GitLab
          </a>{" "}
          or just{" "}
          <button
            className={emailObfuscatorStyles.obfuscator}
            onClick={handleClick}
          >
            say hello
          </button>
          . Let&apos;s talk!
        </p>
      </div>
    </div>
  )
}

export default Index
