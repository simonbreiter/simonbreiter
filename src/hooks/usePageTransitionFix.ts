import Router from "next/router"
import { useEffect } from "react"

// Temporary fix to avoid flash of unstyled content
// during route transitions. Keep an eye on this
// issue and remove this code when resolved:
// https://github.com/vercel/next.js/issues/17464

export const OPACITY_EXIT_DURATION = 1

const routeChange = () => {
  const tempFix = () => {
    const elements = document.querySelectorAll('style[media="x"]')
    elements.forEach((elem) => elem.removeAttribute("media"))
    setTimeout(() => {
      elements.forEach((elem) => elem.remove())
    }, OPACITY_EXIT_DURATION * 1000)
  }
  tempFix()
}

export const usePageTransitionFix = () => {
  useEffect(() => {
    Router.events.on("routeChangeComplete", routeChange)
    Router.events.on("routeChangeStart", routeChange)

    return () => {
      Router.events.off("routeChangeComplete", routeChange)
      Router.events.off("routeChangeStart", routeChange)
    }
  }, [])

  useEffect(() => {
    const asPath = Router.router?.asPath
    Router.router?.push(asPath as any)
    // ? Use replace() instead of push()?
  }, [])
}
