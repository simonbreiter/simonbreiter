import Menu from "./Menu"
import Footer from "@components/Footer"
import Head from "next/head"
import styles from "@styles/layout.module.css"

const Layout = ({ children }: any) => {
  return (
    <div>
      <Head>
        <title>Simon Breiter</title>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta name="description" content="Creating software for humans." />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <div className={styles.grid}>
        <div className={styles.header}>Simon Breiter</div>
        <Menu />

        {children}

        <Footer />
      </div>
    </div>
  )
}

export default Layout
