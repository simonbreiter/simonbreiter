import styles from "@styles/menu.module.css"
import Link from "next/link"
import { useRouter } from "next/router"

const Menu = () => {
  const router = useRouter()
  return (
    <nav className={styles.menu}>
      <ul>
        <li
          className={router.pathname.includes("/projects") ? styles.active : ""}
        >
          <Link href="/projects">
            <a>Projects</a>
          </Link>
        </li>
        <li className={router.pathname.includes("/posts") ? styles.active : ""}>
          <Link href="/posts">
            <a>Posts</a>
          </Link>
        </li>
        <li className={router.pathname.includes("/about") ? styles.active : ""}>
          <Link href="/about">
            <a>About</a>
          </Link>
        </li>
      </ul>
    </nav>
  )
}

export default Menu
