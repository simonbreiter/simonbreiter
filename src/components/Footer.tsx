import styles from "@styles/footer.module.css"

const Footer = () => {
  return (
    <footer className={styles.footer}>
      <a href="https://gitlab.com/simonbreiter/simonbreiter">Open Source</a> and
      made with ❤️ {new Date().getFullYear()}
    </footer>
  )
}

export default Footer
