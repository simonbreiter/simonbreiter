// https://nextjs.org/docs/api-reference/next.config.js/introduction
module.exports = {
  pageExtensions: ["ts", "tsx", "js", "jsx"],
  reactStrictMode: true,
  poweredByHeader: false,
  env: {
    title: "my title",
  },
  async redirects() {
    return [
      {
        source: "/",
        destination: "/projects",
        permanent: false,
      },
    ]
  },
}
